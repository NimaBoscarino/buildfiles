#!/bin/sh

BUILD_DIR=${PWD}/build-tor-arm
ZLIB_VERSION=1.2.11
LIBEVENT_VERSION=2.1.8-stable
OPENSSL_VERSION=1.0.2p
TOR_VERSION=0.3.5.3-alpha

mkdir ${BUILD_DIR} 
cd ${BUILD_DIR}

curl -fsSL "https://zlib.net/zlib-${ZLIB_VERSION}.tar.gz" | tar zxvf - 
cd zlib-${ZLIB_VERSION} 
./configure --prefix=$PWD/install 
make -j$(nproc) 
make install 
cd ..

curl -fsSL "https://github.com/libevent/libevent/releases/download/release-${LIBEVENT_VERSION}/libevent-${LIBEVENT_VERSION}.tar.gz" | tar zxvf - 
cd libevent-${LIBEVENT_VERSION} 
./configure --prefix=$PWD/install \
           --disable-shared \
           --enable-static \
           --with-pic \
	   --host=arm-linux-gnueabihf 
make -j$(nproc)
make install
cd ..

curl -fsSL "https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz" | tar zxvf - 
cd openssl-${OPENSSL_VERSION}
./Configure --openssldir=$PWD/install no-shared no-dso linux-armv4
make -j$(nproc) INSTALL_PREFIX=$PWD/install
make install
cd .. 

curl -fsSL "https://www.torproject.org/dist/tor-${TOR_VERSION}.tar.gz" | tar zxvf -
cd tor-${TOR_VERSION}
./configure --prefix=$PWD/install \
            --enable-static-tor \
            --with-libevent-dir=$PWD/../libevent-${LIBEVENT_VERSION}/install \
            --with-openssl-dir=$PWD/../openssl-${OPENSSL_VERSION}/install \
            --with-zlib-dir=$PWD/../zlib-${ZLIB_VERSION}/install \
	    --host=armv7-unknown-linux-gnueabi \
	    --disable-tool-name-check 
make -j$(nproc)
make install

echo ""
echo "Results in ${BUILD_DIR}/tor-${TOR_VERSION}/install/bin"
